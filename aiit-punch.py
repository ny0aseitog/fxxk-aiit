import grequests
import datetime
import ntplib
import time

def punch(stuID_list):
    url='http://ims.aiit.edu.cn/signMobile/saveSign.do'
    headers={
        'Accept-Encoding': 'gzip, deflate',
        'User-Agent':'okhttp/3.10.0',
        'Connection': 'close'
    }
    def multipart(stuID):
        files={
            '_userCode':(None,stuID)
        }
        return files
    res=[]
    for tmp in stuID_list:
        res.append(grequests.post(url=url,headers=headers,files=multipart(stuID=tmp)))
    res_list=grequests.map(res)
    return res_list

def now_time():
    unix_timestamp=ntplib.NTPClient().request('ntp.aliyun.com').tx_time
    hours=datetime.datetime.fromtimestamp(unix_timestamp).hour
    return hours

if __name__=='__main__':
    while True:
        hour=now_time()
        if hour==9 or hour==8 or hour==10 or hour==11:
            punch(['319xxxxxxxxx','320xxxxxxxxx'])#在此处填写你的学号，如果多个人，就写多个人的学号
            print('punch success')
            time.sleep(86400)
        else:
            print("wait.....")
            time.sleep(3600)
            
